import { NotificationsServiceInterface } from './interfaces/notifications.interface';
import { Observable } from 'rxjs';
export declare class NotificationsService implements NotificationsServiceInterface {
    private _notifications;
    private subscribers;
    notificationsObservable(): Observable<any[]>;
    add(notification: any, closeAfterShow?: boolean): void;
    remove(notificationIndex: any): void;
    removeLastAfter(interval: number): void;
}
