import { Observable } from 'rxjs';
export interface NotificationsServiceInterface {
    /**
     * Return notifications from service
     */
    notificationsObservable(): Observable<any[]>;
    /**
     * Add new notification in service
     * @param notification
     * @param closeAfterShow
     */
    add(notification: any, closeAfterShow?: boolean): void;
    /**
     * Remove notification from service
     * @param notificationIndex
     */
    remove(notificationIndex: any): void;
}
