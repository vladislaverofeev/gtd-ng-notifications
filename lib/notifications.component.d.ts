import { NotificationsServiceInterface } from './services/interfaces/notifications.interface';
import { Observable } from "rxjs";
export declare class NotificationsComponent {
    private notificationsService;
    notifications$: Observable<any[]>;
    constructor(notificationsService: NotificationsServiceInterface);
    close(index: any): void;
    trackByFn(index: any, item: any): any;
}
