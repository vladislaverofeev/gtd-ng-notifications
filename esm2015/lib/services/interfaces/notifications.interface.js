/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/**
 * @record
 */
export function NotificationsServiceInterface() { }
if (false) {
    /**
     * Return notifications from service
     * @return {?}
     */
    NotificationsServiceInterface.prototype.notificationsObservable = function () { };
    /**
     * Add new notification in service
     * @param {?} notification
     * @param {?=} closeAfterShow
     * @return {?}
     */
    NotificationsServiceInterface.prototype.add = function (notification, closeAfterShow) { };
    /**
     * Remove notification from service
     * @param {?} notificationIndex
     * @return {?}
     */
    NotificationsServiceInterface.prototype.remove = function (notificationIndex) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9ucy5pbnRlcmZhY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ndGQtbmctbm90aWZpY2F0aW9ucy8iLCJzb3VyY2VzIjpbImxpYi9zZXJ2aWNlcy9pbnRlcmZhY2VzL25vdGlmaWNhdGlvbnMuaW50ZXJmYWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7QUFFQSxtREFvQkM7Ozs7OztJQWZBLGtGQUE2Qzs7Ozs7OztJQU83QywwRkFBa0Q7Ozs7OztJQU1sRCxrRkFBZ0MiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQge09ic2VydmFibGV9IGZyb20gJ3J4anMnO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBOb3RpZmljYXRpb25zU2VydmljZUludGVyZmFjZSB7XHJcblxyXG5cdC8qKlxyXG5cdCAqIFJldHVybiBub3RpZmljYXRpb25zIGZyb20gc2VydmljZVxyXG5cdCAqL1xyXG5cdG5vdGlmaWNhdGlvbnNPYnNlcnZhYmxlKCk6IE9ic2VydmFibGU8YW55W10+O1xyXG5cclxuXHQvKipcclxuXHQgKiBBZGQgbmV3IG5vdGlmaWNhdGlvbiBpbiBzZXJ2aWNlXHJcblx0ICogQHBhcmFtIG5vdGlmaWNhdGlvblxyXG5cdCAqIEBwYXJhbSBjbG9zZUFmdGVyU2hvd1xyXG5cdCAqL1xyXG5cdGFkZChub3RpZmljYXRpb24sIGNsb3NlQWZ0ZXJTaG93PzogYm9vbGVhbik6IHZvaWQ7XHJcblxyXG5cdC8qKlxyXG5cdCAqIFJlbW92ZSBub3RpZmljYXRpb24gZnJvbSBzZXJ2aWNlXHJcblx0ICogQHBhcmFtIG5vdGlmaWNhdGlvbkluZGV4XHJcblx0ICovXHJcblx0cmVtb3ZlKG5vdGlmaWNhdGlvbkluZGV4KTogdm9pZDtcclxuXHJcbn0iXX0=