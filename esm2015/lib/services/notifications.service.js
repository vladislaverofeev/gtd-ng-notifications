/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { BehaviorSubject, timer } from 'rxjs';
export class NotificationsService {
    constructor() {
        this._notifications = new BehaviorSubject([]);
        this.subscribers = {};
    }
    /**
     * @return {?}
     */
    notificationsObservable() {
        return this._notifications.asObservable();
    }
    /**
     * @param {?} notification
     * @param {?=} closeAfterShow
     * @return {?}
     */
    add(notification, closeAfterShow = false) {
        /** @type {?} */
        let notifications = this._notifications.getValue();
        notifications.unshift(notification);
        this._notifications.next(notifications);
        if (closeAfterShow) {
            this.removeLastAfter(10000);
        }
    }
    /**
     * @param {?} notificationIndex
     * @return {?}
     */
    remove(notificationIndex) {
        /** @type {?} */
        let notifications = this._notifications.getValue();
        notifications.splice(notificationIndex, 1);
        this._notifications.next(notifications);
    }
    /**
     * @param {?} interval
     * @return {?}
     */
    removeLastAfter(interval) {
        this.subscribers.timer = timer(interval).subscribe((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            const currentNotifications = this._notifications.getValue();
            /** @type {?} */
            const index = currentNotifications.length - 1;
            if (index >= 0) {
                this.remove(index);
            }
        }));
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    NotificationsService.prototype._notifications;
    /**
     * @type {?}
     * @private
     */
    NotificationsService.prototype.subscribers;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9ucy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZ3RkLW5nLW5vdGlmaWNhdGlvbnMvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvbm90aWZpY2F0aW9ucy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFDQSxPQUFPLEVBQUMsZUFBZSxFQUFjLEtBQUssRUFBQyxNQUFNLE1BQU0sQ0FBQztBQUV4RCxNQUFNLE9BQU8sb0JBQW9CO0lBQWpDO1FBRVMsbUJBQWMsR0FBMkIsSUFBSSxlQUFlLENBQVEsRUFBRSxDQUFDLENBQUM7UUFDeEUsZ0JBQVcsR0FBUSxFQUFFLENBQUM7SUFnQy9CLENBQUM7Ozs7SUE5QkEsdUJBQXVCO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUMzQyxDQUFDOzs7Ozs7SUFFRCxHQUFHLENBQUMsWUFBWSxFQUFFLGlCQUEwQixLQUFLOztZQUM1QyxhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEVBQUU7UUFDbEQsYUFBYSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUNwQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUV4QyxJQUFJLGNBQWMsRUFBRTtZQUNuQixJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzVCO0lBQ0YsQ0FBQzs7Ozs7SUFFRCxNQUFNLENBQUMsaUJBQWlCOztZQUNuQixhQUFhLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEVBQUU7UUFDbEQsYUFBYSxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLENBQUMsQ0FBQztRQUMzQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUN6QyxDQUFDOzs7OztJQUVELGVBQWUsQ0FBQyxRQUFnQjtRQUMvQixJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsU0FBUzs7O1FBQUMsR0FBRyxFQUFFOztrQkFDakQsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEVBQUU7O2tCQUNyRCxLQUFLLEdBQUcsb0JBQW9CLENBQUMsTUFBTSxHQUFHLENBQUM7WUFDN0MsSUFBSSxLQUFLLElBQUksQ0FBQyxFQUFFO2dCQUNmLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDbkI7UUFDRixDQUFDLEVBQUMsQ0FBQztJQUNKLENBQUM7Q0FFRDs7Ozs7O0lBakNBLDhDQUFnRjs7Ozs7SUFDaEYsMkNBQThCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOb3RpZmljYXRpb25zU2VydmljZUludGVyZmFjZX0gZnJvbSAnLi9pbnRlcmZhY2VzL25vdGlmaWNhdGlvbnMuaW50ZXJmYWNlJztcclxuaW1wb3J0IHtCZWhhdmlvclN1YmplY3QsIE9ic2VydmFibGUsIHRpbWVyfSBmcm9tICdyeGpzJztcclxuXHJcbmV4cG9ydCBjbGFzcyBOb3RpZmljYXRpb25zU2VydmljZSBpbXBsZW1lbnRzIE5vdGlmaWNhdGlvbnNTZXJ2aWNlSW50ZXJmYWNlIHtcclxuXHJcblx0cHJpdmF0ZSBfbm90aWZpY2F0aW9uczogQmVoYXZpb3JTdWJqZWN0PGFueVtdPiA9IG5ldyBCZWhhdmlvclN1YmplY3Q8YW55W10+KFtdKTtcdFxyXG5cdHByaXZhdGUgc3Vic2NyaWJlcnM6IGFueSA9IHt9O1xyXG5cclxuXHRub3RpZmljYXRpb25zT2JzZXJ2YWJsZSgpOiBPYnNlcnZhYmxlPGFueVtdPiB7XHJcblx0XHRyZXR1cm4gdGhpcy5fbm90aWZpY2F0aW9ucy5hc09ic2VydmFibGUoKTtcclxuXHR9XHJcblxyXG5cdGFkZChub3RpZmljYXRpb24sIGNsb3NlQWZ0ZXJTaG93OiBib29sZWFuID0gZmFsc2UpOiB2b2lkIHtcclxuXHRcdGxldCBub3RpZmljYXRpb25zID0gdGhpcy5fbm90aWZpY2F0aW9ucy5nZXRWYWx1ZSgpO1xyXG5cdFx0bm90aWZpY2F0aW9ucy51bnNoaWZ0KG5vdGlmaWNhdGlvbik7XHJcblx0XHR0aGlzLl9ub3RpZmljYXRpb25zLm5leHQobm90aWZpY2F0aW9ucyk7XHJcblxyXG5cdFx0aWYgKGNsb3NlQWZ0ZXJTaG93KSB7XHJcblx0XHRcdHRoaXMucmVtb3ZlTGFzdEFmdGVyKDEwMDAwKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHJlbW92ZShub3RpZmljYXRpb25JbmRleCk6IHZvaWQge1xyXG5cdFx0bGV0IG5vdGlmaWNhdGlvbnMgPSB0aGlzLl9ub3RpZmljYXRpb25zLmdldFZhbHVlKCk7XHJcblx0XHRub3RpZmljYXRpb25zLnNwbGljZShub3RpZmljYXRpb25JbmRleCwgMSk7XHJcblx0XHR0aGlzLl9ub3RpZmljYXRpb25zLm5leHQobm90aWZpY2F0aW9ucyk7XHJcblx0fVxyXG5cdFxyXG5cdHJlbW92ZUxhc3RBZnRlcihpbnRlcnZhbDogbnVtYmVyKTogdm9pZCB7XHJcblx0XHR0aGlzLnN1YnNjcmliZXJzLnRpbWVyID0gdGltZXIoaW50ZXJ2YWwpLnN1YnNjcmliZSgoKSA9PiB7XHJcblx0XHRcdGNvbnN0IGN1cnJlbnROb3RpZmljYXRpb25zID0gdGhpcy5fbm90aWZpY2F0aW9ucy5nZXRWYWx1ZSgpO1xyXG5cdFx0XHRjb25zdCBpbmRleCA9IGN1cnJlbnROb3RpZmljYXRpb25zLmxlbmd0aCAtIDE7XHJcblx0XHRcdGlmIChpbmRleCA+PSAwKSB7XHJcblx0XHRcdFx0dGhpcy5yZW1vdmUoaW5kZXgpO1xyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG59XHJcbiJdfQ==