/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component, Inject } from '@angular/core';
export class NotificationsComponent {
    /**
     * @param {?} notificationsService
     */
    constructor(notificationsService) {
        this.notificationsService = notificationsService;
        this.notifications$ = this.notificationsService.notificationsObservable();
    }
    /**
     * @param {?} index
     * @return {?}
     */
    close(index) {
        this.notificationsService.remove(index);
    }
    /**
     * @param {?} index
     * @param {?} item
     * @return {?}
     */
    trackByFn(index, item) {
        return index;
    }
}
NotificationsComponent.decorators = [
    { type: Component, args: [{
                selector: 'float-notifications',
                template: "<div class=\"section float-notifications\" *ngIf=\"(notifications$ | async) as notifications\">\r\n\t<div class=\"notifications\">\r\n\t\t<ng-container *ngFor=\"let item of notifications; let i = index; trackBy: trackByFn\">\r\n\t\t\t<div class=\"notifications__item mb-d-sm-2\" [ngClass]=\"{ 'notifications__item--success': (item.type === 'success'), 'notifications__item--error': (item.type === 'error') }\">\r\n\t\t\t\t<div class=\"notifications__content\">{{ item.content }}</div>\r\n\t\t\t\t<button type=\"button\" class=\"notifications__close\" (click)=\"close(i)\">\r\n\t\t\t\t\t<i class=\"icon icon-close\"></i>\r\n\t\t\t\t</button>\r\n\t\t\t</div>\r\n\t\t</ng-container>\r\n\t</div>\r\n</div>"
            }] }
];
/** @nocollapse */
NotificationsComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: ['NotificationsServiceInterface',] }] }
];
if (false) {
    /** @type {?} */
    NotificationsComponent.prototype.notifications$;
    /**
     * @type {?}
     * @private
     */
    NotificationsComponent.prototype.notificationsService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9ucy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9ndGQtbmctbm90aWZpY2F0aW9ucy8iLCJzb3VyY2VzIjpbImxpYi9ub3RpZmljYXRpb25zLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7O0FBQUEsT0FBTyxFQUFDLFNBQVMsRUFBRSxNQUFNLEVBQUMsTUFBTSxlQUFlLENBQUM7QUFRaEQsTUFBTSxPQUFPLHNCQUFzQjs7OztJQUsvQixZQUE2RCxvQkFBbUQ7UUFBbkQseUJBQW9CLEdBQXBCLG9CQUFvQixDQUErQjtRQUM1RyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyx1QkFBdUIsRUFBRSxDQUFDO0lBQzlFLENBQUM7Ozs7O0lBRUQsS0FBSyxDQUFDLEtBQUs7UUFDUCxJQUFJLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzVDLENBQUM7Ozs7OztJQUVELFNBQVMsQ0FBQyxLQUFLLEVBQUUsSUFBSTtRQUNwQixPQUFPLEtBQUssQ0FBQztJQUNkLENBQUM7OztZQW5CSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLHFCQUFxQjtnQkFDL0IseXNCQUE2QzthQUNoRDs7Ozs0Q0FNZ0IsTUFBTSxTQUFDLCtCQUErQjs7OztJQUhuRCxnREFBeUM7Ozs7O0lBRzdCLHNEQUFvRyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7Q29tcG9uZW50LCBJbmplY3R9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQge05vdGlmaWNhdGlvbnNTZXJ2aWNlSW50ZXJmYWNlfSBmcm9tICcuL3NlcnZpY2VzL2ludGVyZmFjZXMvbm90aWZpY2F0aW9ucy5pbnRlcmZhY2UnO1xyXG5pbXBvcnQge09ic2VydmFibGV9IGZyb20gXCJyeGpzXCI7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnZmxvYXQtbm90aWZpY2F0aW9ucycsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vbm90aWZpY2F0aW9ucy5jb21wb25lbnQuaHRtbCdcclxufSlcclxuZXhwb3J0IGNsYXNzIE5vdGlmaWNhdGlvbnNDb21wb25lbnQge1xyXG5cclxuICAgIHB1YmxpYyBub3RpZmljYXRpb25zJDogT2JzZXJ2YWJsZTxhbnlbXT47XHJcblxyXG5cclxuICAgIGNvbnN0cnVjdG9yKEBJbmplY3QoJ05vdGlmaWNhdGlvbnNTZXJ2aWNlSW50ZXJmYWNlJykgcHJpdmF0ZSBub3RpZmljYXRpb25zU2VydmljZTogTm90aWZpY2F0aW9uc1NlcnZpY2VJbnRlcmZhY2UpIHtcclxuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbnMkID0gdGhpcy5ub3RpZmljYXRpb25zU2VydmljZS5ub3RpZmljYXRpb25zT2JzZXJ2YWJsZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGNsb3NlKGluZGV4KSB7XHJcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25zU2VydmljZS5yZW1vdmUoaW5kZXgpO1xyXG4gICAgfVxyXG5cclxuICAgIHRyYWNrQnlGbihpbmRleCwgaXRlbSkge1xyXG4gICAgXHRyZXR1cm4gaW5kZXg7XHJcbiAgICB9XHJcblxyXG59Il19