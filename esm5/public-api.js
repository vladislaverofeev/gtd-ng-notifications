/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*
 * Public API Surface of gtd-ng-table
 */
export { NotificationsModule } from './lib//notifications.module';
export { NotificationsComponent } from './lib/notifications.component';
export { NotificationsService } from './lib/services/notifications.service';
export {} from './lib/services/interfaces/notifications.interface';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL2d0ZC1uZy1ub3RpZmljYXRpb25zLyIsInNvdXJjZXMiOlsicHVibGljLWFwaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7O0FBSUEsb0NBQWMsNkJBQTZCLENBQUM7QUFDNUMsdUNBQWMsK0JBQStCLENBQUM7QUFDOUMscUNBQWMsc0NBQXNDLENBQUM7QUFDckQsZUFBYyxtREFBbUQsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIi8qXHJcbiAqIFB1YmxpYyBBUEkgU3VyZmFjZSBvZiBndGQtbmctdGFibGVcclxuICovXHJcblxyXG5leHBvcnQgKiBmcm9tICcuL2xpYi8vbm90aWZpY2F0aW9ucy5tb2R1bGUnO1xyXG5leHBvcnQgKiBmcm9tICcuL2xpYi9ub3RpZmljYXRpb25zLmNvbXBvbmVudCc7XHJcbmV4cG9ydCAqIGZyb20gJy4vbGliL3NlcnZpY2VzL25vdGlmaWNhdGlvbnMuc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vbGliL3NlcnZpY2VzL2ludGVyZmFjZXMvbm90aWZpY2F0aW9ucy5pbnRlcmZhY2UnO1xyXG4iXX0=