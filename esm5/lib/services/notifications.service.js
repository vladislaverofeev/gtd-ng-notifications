/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { BehaviorSubject, timer } from 'rxjs';
var NotificationsService = /** @class */ (function () {
    function NotificationsService() {
        this._notifications = new BehaviorSubject([]);
        this.subscribers = {};
    }
    /**
     * @return {?}
     */
    NotificationsService.prototype.notificationsObservable = /**
     * @return {?}
     */
    function () {
        return this._notifications.asObservable();
    };
    /**
     * @param {?} notification
     * @param {?=} closeAfterShow
     * @return {?}
     */
    NotificationsService.prototype.add = /**
     * @param {?} notification
     * @param {?=} closeAfterShow
     * @return {?}
     */
    function (notification, closeAfterShow) {
        if (closeAfterShow === void 0) { closeAfterShow = false; }
        /** @type {?} */
        var notifications = this._notifications.getValue();
        notifications.unshift(notification);
        this._notifications.next(notifications);
        if (closeAfterShow) {
            this.removeLastAfter(10000);
        }
    };
    /**
     * @param {?} notificationIndex
     * @return {?}
     */
    NotificationsService.prototype.remove = /**
     * @param {?} notificationIndex
     * @return {?}
     */
    function (notificationIndex) {
        /** @type {?} */
        var notifications = this._notifications.getValue();
        notifications.splice(notificationIndex, 1);
        this._notifications.next(notifications);
    };
    /**
     * @param {?} interval
     * @return {?}
     */
    NotificationsService.prototype.removeLastAfter = /**
     * @param {?} interval
     * @return {?}
     */
    function (interval) {
        var _this = this;
        this.subscribers.timer = timer(interval).subscribe((/**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var currentNotifications = _this._notifications.getValue();
            /** @type {?} */
            var index = currentNotifications.length - 1;
            if (index >= 0) {
                _this.remove(index);
            }
        }));
    };
    return NotificationsService;
}());
export { NotificationsService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    NotificationsService.prototype._notifications;
    /**
     * @type {?}
     * @private
     */
    NotificationsService.prototype.subscribers;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9ucy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vZ3RkLW5nLW5vdGlmaWNhdGlvbnMvIiwic291cmNlcyI6WyJsaWIvc2VydmljZXMvbm90aWZpY2F0aW9ucy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7QUFDQSxPQUFPLEVBQUMsZUFBZSxFQUFjLEtBQUssRUFBQyxNQUFNLE1BQU0sQ0FBQztBQUV4RDtJQUFBO1FBRVMsbUJBQWMsR0FBMkIsSUFBSSxlQUFlLENBQVEsRUFBRSxDQUFDLENBQUM7UUFDeEUsZ0JBQVcsR0FBUSxFQUFFLENBQUM7SUFnQy9CLENBQUM7Ozs7SUE5QkEsc0RBQXVCOzs7SUFBdkI7UUFDQyxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDM0MsQ0FBQzs7Ozs7O0lBRUQsa0NBQUc7Ozs7O0lBQUgsVUFBSSxZQUFZLEVBQUUsY0FBK0I7UUFBL0IsK0JBQUEsRUFBQSxzQkFBK0I7O1lBQzVDLGFBQWEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRTtRQUNsRCxhQUFhLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO1FBQ3BDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBRXhDLElBQUksY0FBYyxFQUFFO1lBQ25CLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDNUI7SUFDRixDQUFDOzs7OztJQUVELHFDQUFNOzs7O0lBQU4sVUFBTyxpQkFBaUI7O1lBQ25CLGFBQWEsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRTtRQUNsRCxhQUFhLENBQUMsTUFBTSxDQUFDLGlCQUFpQixFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzNDLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQ3pDLENBQUM7Ozs7O0lBRUQsOENBQWU7Ozs7SUFBZixVQUFnQixRQUFnQjtRQUFoQyxpQkFRQztRQVBBLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxTQUFTOzs7UUFBQzs7Z0JBQzVDLG9CQUFvQixHQUFHLEtBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxFQUFFOztnQkFDckQsS0FBSyxHQUFHLG9CQUFvQixDQUFDLE1BQU0sR0FBRyxDQUFDO1lBQzdDLElBQUksS0FBSyxJQUFJLENBQUMsRUFBRTtnQkFDZixLQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ25CO1FBQ0YsQ0FBQyxFQUFDLENBQUM7SUFDSixDQUFDO0lBRUYsMkJBQUM7QUFBRCxDQUFDLEFBbkNELElBbUNDOzs7Ozs7O0lBakNBLDhDQUFnRjs7Ozs7SUFDaEYsMkNBQThCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtOb3RpZmljYXRpb25zU2VydmljZUludGVyZmFjZX0gZnJvbSAnLi9pbnRlcmZhY2VzL25vdGlmaWNhdGlvbnMuaW50ZXJmYWNlJztcclxuaW1wb3J0IHtCZWhhdmlvclN1YmplY3QsIE9ic2VydmFibGUsIHRpbWVyfSBmcm9tICdyeGpzJztcclxuXHJcbmV4cG9ydCBjbGFzcyBOb3RpZmljYXRpb25zU2VydmljZSBpbXBsZW1lbnRzIE5vdGlmaWNhdGlvbnNTZXJ2aWNlSW50ZXJmYWNlIHtcclxuXHJcblx0cHJpdmF0ZSBfbm90aWZpY2F0aW9uczogQmVoYXZpb3JTdWJqZWN0PGFueVtdPiA9IG5ldyBCZWhhdmlvclN1YmplY3Q8YW55W10+KFtdKTtcdFxyXG5cdHByaXZhdGUgc3Vic2NyaWJlcnM6IGFueSA9IHt9O1xyXG5cclxuXHRub3RpZmljYXRpb25zT2JzZXJ2YWJsZSgpOiBPYnNlcnZhYmxlPGFueVtdPiB7XHJcblx0XHRyZXR1cm4gdGhpcy5fbm90aWZpY2F0aW9ucy5hc09ic2VydmFibGUoKTtcclxuXHR9XHJcblxyXG5cdGFkZChub3RpZmljYXRpb24sIGNsb3NlQWZ0ZXJTaG93OiBib29sZWFuID0gZmFsc2UpOiB2b2lkIHtcclxuXHRcdGxldCBub3RpZmljYXRpb25zID0gdGhpcy5fbm90aWZpY2F0aW9ucy5nZXRWYWx1ZSgpO1xyXG5cdFx0bm90aWZpY2F0aW9ucy51bnNoaWZ0KG5vdGlmaWNhdGlvbik7XHJcblx0XHR0aGlzLl9ub3RpZmljYXRpb25zLm5leHQobm90aWZpY2F0aW9ucyk7XHJcblxyXG5cdFx0aWYgKGNsb3NlQWZ0ZXJTaG93KSB7XHJcblx0XHRcdHRoaXMucmVtb3ZlTGFzdEFmdGVyKDEwMDAwKTtcclxuXHRcdH1cclxuXHR9XHJcblxyXG5cdHJlbW92ZShub3RpZmljYXRpb25JbmRleCk6IHZvaWQge1xyXG5cdFx0bGV0IG5vdGlmaWNhdGlvbnMgPSB0aGlzLl9ub3RpZmljYXRpb25zLmdldFZhbHVlKCk7XHJcblx0XHRub3RpZmljYXRpb25zLnNwbGljZShub3RpZmljYXRpb25JbmRleCwgMSk7XHJcblx0XHR0aGlzLl9ub3RpZmljYXRpb25zLm5leHQobm90aWZpY2F0aW9ucyk7XHJcblx0fVxyXG5cdFxyXG5cdHJlbW92ZUxhc3RBZnRlcihpbnRlcnZhbDogbnVtYmVyKTogdm9pZCB7XHJcblx0XHR0aGlzLnN1YnNjcmliZXJzLnRpbWVyID0gdGltZXIoaW50ZXJ2YWwpLnN1YnNjcmliZSgoKSA9PiB7XHJcblx0XHRcdGNvbnN0IGN1cnJlbnROb3RpZmljYXRpb25zID0gdGhpcy5fbm90aWZpY2F0aW9ucy5nZXRWYWx1ZSgpO1xyXG5cdFx0XHRjb25zdCBpbmRleCA9IGN1cnJlbnROb3RpZmljYXRpb25zLmxlbmd0aCAtIDE7XHJcblx0XHRcdGlmIChpbmRleCA+PSAwKSB7XHJcblx0XHRcdFx0dGhpcy5yZW1vdmUoaW5kZXgpO1xyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHR9XHJcblxyXG59XHJcbiJdfQ==