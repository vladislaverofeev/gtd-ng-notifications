import { Component, Inject, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { BehaviorSubject, timer } from 'rxjs';

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var NotificationsComponent = /** @class */ (function () {
    function NotificationsComponent(notificationsService) {
        this.notificationsService = notificationsService;
        this.notifications$ = this.notificationsService.notificationsObservable();
    }
    /**
     * @param {?} index
     * @return {?}
     */
    NotificationsComponent.prototype.close = /**
     * @param {?} index
     * @return {?}
     */
    function (index) {
        this.notificationsService.remove(index);
    };
    /**
     * @param {?} index
     * @param {?} item
     * @return {?}
     */
    NotificationsComponent.prototype.trackByFn = /**
     * @param {?} index
     * @param {?} item
     * @return {?}
     */
    function (index, item) {
        return index;
    };
    NotificationsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'float-notifications',
                    template: "<div class=\"section float-notifications\" *ngIf=\"(notifications$ | async) as notifications\">\r\n\t<div class=\"notifications\">\r\n\t\t<ng-container *ngFor=\"let item of notifications; let i = index; trackBy: trackByFn\">\r\n\t\t\t<div class=\"notifications__item mb-d-sm-2\" [ngClass]=\"{ 'notifications__item--success': (item.type === 'success'), 'notifications__item--error': (item.type === 'error') }\">\r\n\t\t\t\t<div class=\"notifications__content\">{{ item.content }}</div>\r\n\t\t\t\t<button type=\"button\" class=\"notifications__close\" (click)=\"close(i)\">\r\n\t\t\t\t\t<i class=\"icon icon-close\"></i>\r\n\t\t\t\t</button>\r\n\t\t\t</div>\r\n\t\t</ng-container>\r\n\t</div>\r\n</div>"
                }] }
    ];
    /** @nocollapse */
    NotificationsComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: ['NotificationsServiceInterface',] }] }
    ]; };
    return NotificationsComponent;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var NotificationsModule = /** @class */ (function () {
    function NotificationsModule() {
    }
    NotificationsModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [
                        NotificationsComponent,
                    ],
                    imports: [
                        CommonModule,
                        BrowserModule
                    ],
                    exports: [NotificationsComponent]
                },] }
    ];
    return NotificationsModule;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var NotificationsService = /** @class */ (function () {
    function NotificationsService() {
        this._notifications = new BehaviorSubject([]);
        this.subscribers = {};
    }
    /**
     * @return {?}
     */
    NotificationsService.prototype.notificationsObservable = /**
     * @return {?}
     */
    function () {
        return this._notifications.asObservable();
    };
    /**
     * @param {?} notification
     * @param {?=} closeAfterShow
     * @return {?}
     */
    NotificationsService.prototype.add = /**
     * @param {?} notification
     * @param {?=} closeAfterShow
     * @return {?}
     */
    function (notification, closeAfterShow) {
        if (closeAfterShow === void 0) { closeAfterShow = false; }
        /** @type {?} */
        var notifications = this._notifications.getValue();
        notifications.unshift(notification);
        this._notifications.next(notifications);
        if (closeAfterShow) {
            this.removeLastAfter(10000);
        }
    };
    /**
     * @param {?} notificationIndex
     * @return {?}
     */
    NotificationsService.prototype.remove = /**
     * @param {?} notificationIndex
     * @return {?}
     */
    function (notificationIndex) {
        /** @type {?} */
        var notifications = this._notifications.getValue();
        notifications.splice(notificationIndex, 1);
        this._notifications.next(notifications);
    };
    /**
     * @param {?} interval
     * @return {?}
     */
    NotificationsService.prototype.removeLastAfter = /**
     * @param {?} interval
     * @return {?}
     */
    function (interval) {
        var _this = this;
        this.subscribers.timer = timer(interval).subscribe((/**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var currentNotifications = _this._notifications.getValue();
            /** @type {?} */
            var index = currentNotifications.length - 1;
            if (index >= 0) {
                _this.remove(index);
            }
        }));
    };
    return NotificationsService;
}());

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { NotificationsModule, NotificationsComponent, NotificationsService };

//# sourceMappingURL=gtd-ng-notifications.js.map